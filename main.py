import argparse
import sys
import bitarray
import os.path
import cv2
import numpy as np

from watermark import insertWatermark, extractWatermark

# Нужно искать макс. AC коэфф, в блоках с макс. мощностью

def getParser():
    parser = argparse.ArgumentParser(description='Insert / Get phrases to the image, using Cox algorithm')
    parser.add_argument('-input', type=str, help='Path to input file')
    parser.add_argument('-insert', action='store_true', help='Flag for insertion')
    parser.add_argument('-extract', action='store_true', help='Flag for extraction')
    parser.add_argument('-src', type=str, help='Path to src img for extraction')
    parser.add_argument('-text', type=str, help='Text to be inserted')
    parser.add_argument('-alpha', default=0.01, type=float, help='Intensity of watermark')
    parser.add_argument('-jpeg',  default=80, type=int, help='Comression quality from 10 to 100 of actual image')

    return parser.parse_args()


if __name__ == '__main__':
    args = getParser()

    if not args.input or not os.path.isfile(args.input):
        print('Please provide proper input file with -input')
        sys.exit() 

    # Read the image
    image = cv2.imread(args.input, 0)

    # divide the image by 255.0
    floatImg = np.float64(image) # / 255.0

    if args.insert:
        if not args.text:
            print('Please provide text with -text')
            sys.exit() 

        wat = insertWatermark(floatImg, args.text, args.alpha)

        # Compress with jpeg to test method of insertion
        cv2.imwrite('wat.jpg', wat, [int(cv2.IMWRITE_JPEG_QUALITY), args.jpeg])
    elif args.extract:
        if not args.src:
            print('Please provide source image for extraction using -src')
            sys.exit()

        watermark = extractWatermark(floatImg, np.float64(cv2.imread(args.src, 0)), args.alpha)
        try:
            print(watermark.decode('utf-8'))
        except:
            print(watermark)
            print('Can not decode watermark')
        

    cv2.waitKey()

        