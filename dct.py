import numpy as np 
import math
from math import cos, pi

def dct(img, n):
    csum = 0
    k = 0
    width = len(img[0])
    height = len(img)

    s = (width, height)

    res = np.zeros(s)
    cu = cv = 0
    for i in range(0, height, 8):
        for j in range(0, width, 8):
            for u in range(0, 8):
                for v in range(0, 8):
                    if (u + v < n):
                        if u == 0:
                            cu = 1 / math.sqrt(2)
                        else:
                            cu = 1

                        if v == 0:
                            cv = 1 / math.sqrt(2)
                        else:
                            cv = 1

                        csum = 0
                        for x in range(0, 8):
                            for y in range(0, 8):
                                csum += (img[x+i][y+j] * math.cos(((2*x+1)*u*math.pi)/16)
                                         * math.cos(((2*y+1)*v*math.pi)/16))
                                        
                        res[u + i][v + j] = csum * 0.25 * cu * cv
                    else:
                        res[u + i][v + j] = 0

    return res


def idct(img):
    width = len(img[0])
    height = len(img)

    s = (width, height)

    res = np.zeros(s)

    for i in range(0, height, 8):
        for j in range(0, width, 8):
            for x in range(0, 8):
                for y in range(0, 8):
                    for u in range(0, 8):
                        for v in range(0, 8):
                            if u == 0:
                                cu = 1 / math.sqrt(2)
                            else:
                                cu = 1

                            if v == 0:
                                cv = 1 / math.sqrt(2)
                            else:
                                cv = 1
                            
                            res[x + i][y + j] += (cu * cv * img[u+i][v+j] * cos(((2*x+1)*u*pi)/16) * cos(((2*y+1)*v*pi)/16))
                    
                    res[x + i][y + j] *= 0.25

    return res


def get_T(N):
    s = (N, N)
    T = np.zeros(s)
    for p in range(0, N):
        for q in range(0, N):
            if p == 0:
                coeff = 1 / N
            else:
                coeff = 2 / N
            
            T[p][q] = math.sqrt(coeff) * cos((pi * (2 * q + 1) * p) / (2 * N))

    return T

T = get_T(8)

T = np.asarray(T)

def mat_dct(X):
    Y = np.dot((np.dot(T,X)), T.T)
    return Y

def mat_idct(Y):
    X = np.dot((np.dot(T.T,Y)), T)
    return X

def fulldct(img, N):
    height = len(img)
    width = len(img[0])

    s = (height, width)
    res = np.zeros(s)

    for i in range(0, height, N):
        for j in range(0, width, N):
            mat = img[i:i+N, j:j+N]
            dctMat = mat_dct(mat)
            res[i:i+N, j:j+N] = dctMat

    return res

def fullidct(img, N):
    height = len(img)
    width = len(img[0])

    s = (height, width)
    res = np.zeros(s)

    for i in range(0, height, N):
        for j in range(0, width, N):
            mat = img[i:i+N, j:j+N]
            dctMat = mat_idct(mat)
            res[i:i+N, j:j+N] = dctMat

    return res