from PIL import Image
import bitarray
import numpy as np
import math
import matplotlib.pyplot as plt
from dct import mat_dct, mat_idct, fulldct, fullidct
import random
import string
N = 8  # Block size


def insert(image: Image, message, alpha):
    print("Inserting!")
    height = image.size[0]
    width = image.size[1]
    print("Size", height, width)
    print("Capacity:", width / N * height / N)

    ba = bitarray.bitarray()
    ba.frombytes(message.encode('utf-8') + b'0xE2')
    bits = ba.tolist()

    watImg = image.copy()
    cnt = 0
    for i in range(0, height, N):
        for j in range(0, width, N):
            maxVal = 0
            maxIdx = [0, 0]
            if cnt < len(bits) and cnt < height*width:
                box = (j, i, j + N, i + N)
                subImg = image.crop(box)
                r, g, b = subImg.split()
                dct = mat_dct(r)

                for k in range(0, N):
                    for l in range(0, N):
                        if k == 0 and l == 0:
                            continue

                        if dct[k][l] > maxVal:
                            maxVal = dct[k][l]
                            maxIdx = [k, l]

                s = 1 if bits[cnt] else -1
                dct[maxIdx[0]][maxIdx[1]] = dct[maxIdx[0]
                                                ][maxIdx[1]] * (1 + alpha * s)
                idct = mat_idct(dct)
                red = Image.fromarray(idct).convert(mode='L')
                # if i == 0 and j == 0:
                # print(np.array(r))
                # print(np.array(red))

                subImg = Image.merge("RGB", (red, g, b))
                watImg.paste(subImg, box)
                cnt += 1

    print("Done")
    watImg.save('res/wat.bmp')
    return watImg


def extract(image: Image, watImg: Image):
    print("Extracting!")
    height = image.size[0]
    width = image.size[1]
    cnt = 0
    bits = bitarray.bitarray()
    for i in range(0, height, N):
        for j in range(0, width, N):
            maxVal = 0
            maxIdx = [0, 0]

            box = (j, i, j + N, i + N)
            srcSub = image.crop(box)
            srcRed = srcSub.split()[0]
            watSub = watImg.crop(box)
            watRed = watSub.split()[0]

            dctSrc = mat_dct(srcRed)
            dctWat = mat_dct(watRed)

            for k in range(0, N):
                for l in range(0, N):
                    if k == 0 and l == 0:
                        continue

                    if dctSrc[k][l] > maxVal:
                        maxVal = dctSrc[k][l]
                        maxIdx = [k, l]

            bits.append(1 if dctWat[maxIdx[0]][maxIdx[1]]
                        > dctSrc[maxIdx[0]][maxIdx[1]] else 0)

    return bits


def calculatePSNR(src, chg):
    mse = np.mean((np.array(src) - np.array(chg)) ** 2)
    if mse == 0:
        return 100
    PIXEL_MAX = 255.0

    return 20 * math.log10(PIXEL_MAX / math.sqrt(mse))


def randomword(length):
    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for i in range(length))


def plotPSNRCapacity(image: Image):
    height = image.size[0]
    width = image.size[1]
    capacity = width / N * height / N
    print("Capacity:", capacity)

    alphaArr = np.arange(0.1, 1, 0.1)
    cnt = 0
    for alpha in alphaArr:
        PSNR = []
        percents = np.arange(0.1, 1, 0.1)
        for percent in percents:
            message = randomword((int)(capacity * percent / 8))
            result = insert(image, message, alpha)
            PSNR.append(calculatePSNR(image, result))
        label = 'alpha=' + '{:.1f}'.format(alpha)
        plt.plot(percents * 100, PSNR, label=label)

    plt.xlabel("%")
    plt.ylabel("PSNR")
    plt.title("PSNR(Capacity)")
    plt.legend()
    plt.savefig('plots/PSNRCap.png')


def plotPSNRAlpha(image: Image):
    height = image.size[0]
    width = image.size[1]
    capacity = width / N * height / N
    print("Capacity:", capacity)

    PSNR = []
    message = randomword((int)(capacity / 8))
    alphaArr = np.arange(0.1, 3, 0.1)
    for alpha in alphaArr:
        result = insert(image, message, alpha)
        PSNR.append(calculatePSNR(image, result))

    plt.plot(alphaArr, PSNR, label='plot')
    plt.xlabel('Alpha')
    plt.ylabel('PSNR')
    plt.title('PSNR(Alpha) with full capacity')
    plt.savefig('plots/PSNRalpha.png')


def getErrorsNumber(src, rcv):
    result = 0
    for i in range(len(src)):
        if src[i] != rcv[i]:
            result += 1

    return result


def plotJpeg(image: Image, cap):
    height = image.size[0]
    width = image.size[1]
    capacity = width / N * height / N
    print("Capacity:", capacity)

    errors = []
    message = randomword((int) (cap / 8) - 8 )

    ba = bitarray.bitarray()
    ba.frombytes(message.encode('utf-8') + b'0xE2')
    bits = ba.tolist()

    alphaArr = np.arange(0.1, 10, 1)
    for alpha in alphaArr:
        result = insert(image, message, alpha)
        result.save('res/wat.jpg')
        Jpeg = Image.open('res/wat.jpg')

        extrBits = extract(image, Jpeg)
        extrBits = extrBits.tolist()
        print(len(bits))
        print(len(extrBits))
        errors.append(getErrorsNumber(bits, extrBits))

    plt.plot(alphaArr, errors, label='plot')
    plt.xlabel('Alpha')
    plt.ylabel('Errors')
    plt.title('Errors(Alpha) with ' + str(cap) + ' capacity')
    plt.savefig('plots/AlphaErrors' + str(cap) + '.png')


path = input("Enter path to source container: ")
if path == '':
    path = 'lena.bmp'

image = Image.open(path)
mode = (int) (input("Select mode\n1.Insertion\n2.Extraction\n3.JPEG\n4.Plot everything\n"))

if mode == 1:
    message = input("Enter message: ")
    if message == '':
        message ="For even greater encryption, chain multiple tools together"

    alpha = input("Choose alpha:")
    if alpha == '':
        alpha = 0.1

    insert(image, message, (float) (alpha))
elif mode == 2:
    pathToWat = input("Enter path to watermarked image: ")
    while pathToWat == '':
        pathToWat = input("Enter path to watermarked image: ")
    
    watImg = Image.open(pathToWat)
    bits = extract(image, watImg)
    phrase = bits.tobytes().split(b'0xE2')[0]
    print(phrase)
elif mode == 3:
    alpha = input("Choose alpha:")
    if alpha == '':
        alpha = 0.1

    message ="For even greater encryption, chain multiple tools together"
    watImg = insert(image, message, (float) (alpha))
    watImg.save('res/wat.jpg')
    Jpeg = Image.open('res/wat.jpg')
    bits = extract(image, Jpeg)
    phrase = bits.tobytes().split(b'0xE2')[0]
    print(phrase)
    print(calculatePSNR(image, watImg))
elif mode == 4:
    plotPSNRAlpha(image)
    plotPSNRCapacity(image)
    plotJpeg(image, 4096)
    plotJpeg(image, 120)
else:
    print("Isn't available")




#image = Image.open('lena.bmp')
#message = "For even greater encryption, chain multiple tools together"

#watImg = insert(image, message, 0.1)
#extract(image, watImg)
# watImg.save('res/wat.jpg')
#Jpeg = Image.open('res/wat.jpg')
#extract(image, Jpeg)

# plotPSNRAlpha(image)
# plotPSNRCapacity(image)
# plotJpeg(image, 120)
