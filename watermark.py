import random
import bitarray
import cv2
import numpy as np 

from dct import fullidct, fulldct

N = 8 # Block size

def extractWatermark(image, src, alpha):
    height = len(image)
    width = len(image[0])

    dctImg = fulldct(np.asarray(image), N)
    cv2.imshow("WATERMARKED", dctImg)
    cv2.imshow("WATSOURCE", image)
    dctSrc = fulldct(np.asarray(src), N)
    cv2.imshow("SOURCE", dctSrc)
    cv2.imshow("IMGSOURCE", src)
    diff = dctImg - dctSrc
    cv2.imshow("DIFF", diff / 128)

    cnt = 0
    bits = bitarray.bitarray()
   
    # Get text from watermarked image
    for i in range(0, height, N):
        for j in range(0, width, N):
            if cnt >= 168:
                break
            maxVal = 0
            maxIdx = [0, 0]
            for k in range(i + 1, i + N):
                for l in range(j + 1, j + N):
                    if k == i and l == j:
                        continue
                    
                    if abs(dctSrc[k][l]) > maxVal:
                        maxVal = dctSrc[k][l]
                        maxIdx = [k, l]

            print('wat', dctImg[maxIdx[0]][maxIdx[1]], 'src', dctSrc[maxIdx[0]][maxIdx[1]])
            if dctImg[maxIdx[0]][maxIdx[1]] < dctSrc[maxIdx[0]][maxIdx[1]]:
                bits.append(0)
            else:
                bits.append(1)
            
            #delta = dctImg[maxIdx[0]][maxIdx[1]] - dctSrc[maxIdx[0]][maxIdx[1]]
        
            #if (abs(delta) > alpha):
            #    bits.append(1 if delta > 0 else 0)
            cnt += 1  

    print(bits)
    phrase = bits.tobytes().split(b'0xE2')

    return phrase[0]

def insertWatermark(image, text, alpha):
    height = len(image)
    width = len(image[0])

    dctImg = fulldct(np.asarray(image), N)
    cv2.imshow("DCT IMAGE 1", dctImg / 255)

    image = fullidct(dctImg, N)
    cv2.imshow('Restored original', image / 255)

    ba = bitarray.bitarray()
    ba.frombytes(text.encode('utf-8') + b'0xE2')
   
    bits = ba.tolist()
    print(ba)

    cnt = 0
    watImg = dctImg.copy()
    print(len(bits))
    for i in range(0, height, N):
        for j in range(0, width, N):
            if cnt >= 168:
                break
            #if cnt >= len(bits):
            #    break
            maxVal = 0
            maxIdx = [0, 0]
            for k in range(i + 1, i + N):
                for l in range(j + 1, j + N):                    
                    if abs(watImg[k][l]) > maxVal:
                        maxVal = watImg[k][l]
                        maxIdx = [k, l]
            
            
            print('Before', watImg[maxIdx[0]][maxIdx[1]])
            watImg[maxIdx[0]][maxIdx[1]] = watImg[maxIdx[0]][maxIdx[1]] * (1 + alpha * ( 1 if bits[cnt] else -1 ) )
            print('After', watImg[maxIdx[0]][maxIdx[1]])
            cnt += 1 

    print(ba)
    cv2.imshow("WAT IMAGE", watImg / 255)
    diff = watImg - dctImg
    cv2.imshow("DIFF", diff / 128)
    watermarked = fullidct(watImg, N)

    cv2.imwrite('watermarked.bmp', watermarked)
    cv2.imshow('Restored watermarked', watermarked / 255)

    return watermarked